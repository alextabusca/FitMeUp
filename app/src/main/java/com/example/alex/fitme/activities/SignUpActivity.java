package com.example.alex.fitme.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alex.fitme.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alex on 5/25/2018.
 * Project: FitMeUp
 */

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.input_name)
    EditText nameText;

    @BindView(R.id.input_email)
    EditText emailText;

    @BindView(R.id.input_password)
    EditText passwordText;

    @BindView(R.id.btn_signup)
    Button signUpButton;

    @BindView(R.id.link_login)
    TextView loginLink;

    //User user;
    //private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUp();
            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Finish the registration screen and return to the Login activity
                finish();
            }
        });
    }

    public void onSignUpSuccess() {
        signUpButton.setEnabled(true);

        Intent resultIntent = new Intent();
        //resultIntent.putExtra("name", nameText.getText().toString());
        resultIntent.putExtra("email", emailText.getText().toString());
        resultIntent.putExtra("password", passwordText.getText().toString());
        setResult(RESULT_OK, resultIntent); // result code and Intent data - send here the email and password of the created user

        // return to login activity
        finish();
    }

    public void onSignUpIncorrectCredentials() {
        Toast.makeText(getBaseContext(), "SignUp failed", Toast.LENGTH_LONG).show();
        signUpButton.setEnabled(true);
    }

    //Validating the login data
    public boolean validate() {
        boolean valid = true;

        String name = nameText.getText().toString();
        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            nameText.setError("At least 3 characters!");
            valid = false;
        } else
            nameText.setError(null);

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("Enter a valid email address!");
            valid = false;
        } else
            emailText.setError(null);

        if (password.isEmpty() || password.length() < 4 || password.length() > 20) {
            passwordText.setError("Between 4 and 20 characters");
            valid = false;
        } else
            passwordText.setError(null);

        return valid;
    }


    public void signUp() {

        boolean validData = true;

        if (!validate()) {
            onSignUpIncorrectCredentials();
            validData = false;
        }

        signUpButton.setEnabled(false);

        if (validData) {
            final ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this,
                    R.style.AppTheme_Dark_Dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Creating Account...");
            progressDialog.show();

            new CreateUser().execute();

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            // On complete call either onSignUpSuccess or onSignUpIncorrectCredentials
                            // depending on success
                            onSignUpSuccess();
                            // onSignUpIncorrectCredentials();
                            progressDialog.dismiss();
                        }
                    }, 2500);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class CreateUser extends AsyncTask<Void, Void, String> {
        private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();

            StringBuilder out = new StringBuilder();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);


                if (n > 0) out.append(new String(b, 0, n));
            }

            return out.toString();
        }

        @Override
        protected String doInBackground(Void... params) {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            String name = nameText.getText().toString();
            String email = emailText.getText().toString();
            String password = passwordText.getText().toString();
            HttpPost httpPost = new HttpPost("http://192.168.0.94:8080/users?name=" + name + "&email=" + email + "&password=" + password);
            String text;
            try {
                HttpResponse response = httpClient.execute(httpPost, localContext);

                HttpEntity entity = response.getEntity();

                text = getASCIIContentFromEntity(entity);

            } catch (Exception e) {
                return e.getLocalizedMessage();
            }

            return text;
        }
    }
}
