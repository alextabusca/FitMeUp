package com.example.alex.fitme.models;

/**
 * Created by Alex on 5/25/2018.
 * Project: FitMe
 */
public class User {

    private Long userID;
    private String name;
    private String email;
    private String password;

    public User() {
    }

    public User(Long userID, String name, String email, String password) {
        this.userID = userID;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String toString() {
        return "User: id = " + userID + ", name= " + name + ", email= " + email + ", password= " + password;
    }

}
