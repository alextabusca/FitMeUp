package com.example.alex.fitme.models;

/**
 * Created by Alex on 6/1/2018.
 * Project: FitMe
 */
public class UserMeal {

    private Long userMealID;
    private User user;
    private Meal meal;

    public UserMeal() {
    }

    public UserMeal(User user, Meal meal) {
        this.user = user;
        this.meal = meal;
    }

    public UserMeal(Long userMealID, User user, Meal meal) {
        this.userMealID = userMealID;
        this.user = user;
        this.meal = meal;
    }

    public Long getUserMealID() {
        return userMealID;
    }

    public void setUserMealID(Long userMealID) {
        this.userMealID = userMealID;
    }

    public User getUserDTO() {
        return user;
    }

    public void setUserDTO(User user) {
        this.user = user;
    }

    public Meal getMealDTO() {
        return meal;
    }

    public void setMealDTO(Meal meal) {
        this.meal = meal;
    }
}
