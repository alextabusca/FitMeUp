package com.example.alex.fitme.models;

/**
 * Created by Alex on 5/29/2018.
 * Project: FitMe
 */
public class Meal {

    private Long mealID;
    private String title;
    private String ingredients;
    private String preparation;

    public Meal() {
    }

    public Meal(String title, String ingredients, String preparation) {
        this.title = title;
        this.ingredients = ingredients;
        this.preparation = preparation;
    }


    public Long getMealID() {
        return mealID;
    }

    public void setMealID(Long mealID) {
        this.mealID = mealID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }
}
