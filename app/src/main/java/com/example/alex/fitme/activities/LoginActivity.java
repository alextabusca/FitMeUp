package com.example.alex.fitme.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.example.alex.fitme.R;
import com.example.alex.fitme.models.Admin;
import com.example.alex.fitme.models.User;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Alex on 5/25/2018.
 * Project: FitMeUp
 */

public class LoginActivity extends AppCompatActivity {

    private static final int REQUEST_SignUp = 0;

    @BindView(R.id.input_email)
    EditText emailText;

    @BindView(R.id.input_password)
    EditText passwordText;

    @BindView(R.id.btn_login)
    Button loginButton;

    @BindView(R.id.link_signUp)
    TextView signUpLink;

    User user;
    Admin admin;
    private ObjectMapper objectMapper = new ObjectMapper();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        signUpLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Start the SignUp activity
                Intent startSignUp = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivityForResult(startSignUp, REQUEST_SignUp);
            }
        });
    }

    public void onLoginSuccess() {
        loginButton.setEnabled(true);

        //Start either UserLandingPage or AdminLandingPage
        if (user != null) {
            Intent loginUser = new Intent(this, UserLanding.class);
            loginUser.putExtra("userID", user.getUserID().toString()); //Send the userID to the other activity
            startActivity(loginUser);
        } else if (admin != null) {
            Intent loginAdmin = new Intent(this, AdminLanding.class);
            startActivity(loginAdmin);
        }
    }

    public void onLoginIncorrectData() {
        Toast.makeText(getBaseContext(), "Incorrect credentials", Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
    }

    //Validating the login data
    public boolean validate() {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("Enter a valid email address!");
            valid = false;
        } else
            emailText.setError(null);

        if (password.isEmpty() || password.length() < 4 || password.length() > 20) {
            passwordText.setError("Between 4 and 20 characters");
            valid = false;
        } else
            passwordText.setError(null);

        return valid;
    }

    private void login() {
        boolean validData = true;

        if (!validate()) {
            onLoginIncorrectData();
            validData = false;
        }

        loginButton.setEnabled(true);

        if (validData) {
            final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                    R.style.AppTheme_Dark_Dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Authenticating...");
            progressDialog.show();

            new GetUserLogin().execute();
            new GetAdminLogin().execute();

            new android.os.Handler().
                    postDelayed(
                            new Runnable() {
                                public void run() {
                                    // On complete call either onLoginSuccess or onLoginIncorrectData
                                    onLoginSuccess();
                                    //onLoginIncorrectData();
                                    progressDialog.dismiss();
                                }
                            }, 2500);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SignUp)
            if (resultCode == RESULT_OK) {
                String email = data.getStringExtra("email");
                String password = data.getStringExtra("password");
                emailText.setText(email);
                passwordText.setText(password);
                login();
            }
    }

    @SuppressLint("StaticFieldLeak")
    class GetUserLogin extends AsyncTask<Void, Void, String> {
        private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();

            StringBuilder out = new StringBuilder();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);


                if (n > 0) out.append(new String(b, 0, n));
            }

            return out.toString();
        }

        @Override
        protected String doInBackground(Void... params) {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            String email = emailText.getText().toString();
            String password = passwordText.getText().toString();
            HttpGet httpGet = new HttpGet("http://192.168.0.94:8080/users/" + email + "/" + password);
            String text;
            try {
                HttpResponse response = httpClient.execute(httpGet, localContext);

                HttpEntity entity = response.getEntity();

                text = getASCIIContentFromEntity(entity);

            } catch (Exception e) {
                return e.getLocalizedMessage();
            }

            return text;
        }


        protected void onPostExecute(String results) {
            if (results != null) {
                try {
                    user = objectMapper.readValue(results, User.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class GetAdminLogin extends AsyncTask<Void, Void, String> {
        private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();

            StringBuilder out = new StringBuilder();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);


                if (n > 0) out.append(new String(b, 0, n));
            }

            return out.toString();
        }

        @Override
        protected String doInBackground(Void... params) {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            String email = emailText.getText().toString();
            String password = passwordText.getText().toString();
            HttpGet httpGet = new HttpGet("http://192.168.0.94:8080/admins/" + email + "/" + password);
            String text;
            try {
                HttpResponse response = httpClient.execute(httpGet, localContext);

                HttpEntity entity = response.getEntity();

                text = getASCIIContentFromEntity(entity);

            } catch (Exception e) {
                return e.getLocalizedMessage();
            }

            return text;
        }

        protected void onPostExecute(String results) {
            if (results != null) {
                try {
                    admin = objectMapper.readValue(results, Admin.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
