package com.example.alex.fitme.responses;

import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Alex on 5/28/2018.
 * Project: FitMe
 */
public class HttpGetRequest extends AsyncTask<String, Void, String> {

    private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();

        StringBuilder out = new StringBuilder();
        int n = 1;
        while (n > 0) {
            byte[] b = new byte[4096];
            n = in.read(b);

            if (n > 0) out.append(new String(b, 0, n));
        }

        return out.toString();
    }

    @Override
    protected String doInBackground(String... params) {
        HttpClient httpClient = new DefaultHttpClient();
        HttpContext localContext = new BasicHttpContext();

        String request = params[0];

        HttpGet httpPost = new HttpGet(request);
        String text;
        try {
            HttpResponse response = httpClient.execute(httpPost, localContext);

            HttpEntity entity = response.getEntity();

            text = getASCIIContentFromEntity(entity);

        } catch (Exception e) {
            return e.getLocalizedMessage();
        }

        return text;
    }
}

