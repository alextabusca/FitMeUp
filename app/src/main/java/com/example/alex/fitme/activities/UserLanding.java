package com.example.alex.fitme.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.example.alex.fitme.R;
import com.example.alex.fitme.adapters.GoalListAdapter;
import com.example.alex.fitme.adapters.MealListAdapter;
import com.example.alex.fitme.adapters.WorkoutListAdapter;
import com.example.alex.fitme.models.Goal;
import com.example.alex.fitme.models.Meal;
import com.example.alex.fitme.models.UserMeal;
import com.example.alex.fitme.models.UserWorkout;
import com.example.alex.fitme.models.Workout;
import com.example.alex.fitme.responses.HttpDeleteRequest;
import com.example.alex.fitme.responses.HttpPostRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserLanding extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    static Long userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_landing);

        Intent getUserIDIntent = getIntent();
        userID = Long.parseLong(getUserIDIntent.getStringExtra("userID"));

        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_landing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A placeholder fragment containing a simple goals view.
     */
    public static class GoalsFragment extends Fragment {

        public GoalsFragment() {

        }

        /**
         * Returns a new instance of this fragment
         */
        public static GoalsFragment newInstance() {
            GoalsFragment fragment = new GoalsFragment();
            Bundle args = new Bundle();
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_user_landing_goals, container, false);
            final ListView userGoals = rootView.findViewById(R.id.userGoalsList);

            final HttpPostRequest httpPostRequest = new HttpPostRequest();
            final HttpDeleteRequest httpDeleteRequest = new HttpDeleteRequest();

            FloatingActionButton adminAddGoal = rootView.findViewById(R.id.userAddGoal);
            adminAddGoal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // inflate alert dialog xml
                    LayoutInflater li = LayoutInflater.from(getContext());
                    View dialogView = li.inflate(R.layout.custom_dialog_create_goal, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());

                    // set title
                    alertDialogBuilder.setTitle("Add goal");

                    // set custom dialog icon
                    //alertDialogBuilder.setIcon(R.drawable.ic_launcher);

                    // set custom_dialog_create_user.xmlate_user.xml to alertDialog builder
                    alertDialogBuilder.setView(dialogView);

                    final EditText titleInput = dialogView.findViewById(R.id.dialog_goalTitle_input);
                    final EditText deadlineInput = dialogView.findViewById(R.id.dialog_goalDeadline_input);

                    // set dialog message
                    alertDialogBuilder
                            .setCancelable(false)
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                                            Date parsedate = null;
                                            try {
                                                parsedate = format.parse(String.valueOf(deadlineInput.getText()));
                                            } catch (ParseException e2) {
                                                e2.printStackTrace();
                                            }
                                            java.sql.Date sqlDate = null;
                                            if (parsedate != null) {
                                                sqlDate = new java.sql.Date(parsedate.getTime());
                                            }

                                            String httpPost = "http://192.168.0.94:8080/goals?title=" + titleInput.getText() + "&deadline=" + sqlDate + "&userID=" + userID;

                                            httpPostRequest.execute(httpPost);
                                        }
                                    })
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            });

            @SuppressLint("StaticFieldLeak")
            class GetGoalListByUser extends AsyncTask<Void, Void, String> {
                private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
                    InputStream in = entity.getContent();

                    StringBuilder out = new StringBuilder();
                    int n = 1;
                    while (n > 0) {
                        byte[] b = new byte[4096];
                        n = in.read(b);

                        if (n > 0) out.append(new String(b, 0, n));
                    }

                    return out.toString();
                }

                @Override
                protected String doInBackground(Void... params) {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet("http://192.168.0.94:8080/goals/byUser=" + userID);
                    String text;
                    try {
                        HttpResponse response = httpClient.execute(httpGet, localContext);

                        HttpEntity entity = response.getEntity();

                        text = getASCIIContentFromEntity(entity);

                    } catch (Exception e) {
                        return e.getLocalizedMessage();
                    }

                    return text;
                }

                protected void onPostExecute(String results) {
                    if (results != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            List<Goal> goalList;
                            goalList = objectMapper.readValue(results, new TypeReference<List<Goal>>() {
                            });

                            Log.d("userList:", String.valueOf(goalList));

                            final GoalListAdapter mGoalListAdapter;
                            mGoalListAdapter = new GoalListAdapter(getActivity(), (ArrayList<Goal>) goalList);
                            userGoals.setAdapter(mGoalListAdapter);

                            userGoals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    Goal clickedGoal = mGoalListAdapter.getItem(i);

                                    assert clickedGoal != null;
                                    String deleteGoalRequest = "http://192.168.0.94:8080/goals?goalID=" + clickedGoal.getGoalID();

                                    httpDeleteRequest.execute(deleteGoalRequest);
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            GetGoalListByUser getGoalListByUser = new GetGoalListByUser();
            getGoalListByUser.execute();

            return rootView;
        }
    }

    /**
     * A placeholder fragment containing a simple workouts view.
     */
    public static class WorkoutsFragment extends Fragment {

        public WorkoutsFragment() {

        }

        /**
         * Returns a new instance of this fragment
         */
        public static WorkoutsFragment newInstance() {
            WorkoutsFragment fragment = new WorkoutsFragment();
            Bundle args = new Bundle();
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_user_landing_workouts, container, false);
            final ListView savedWorkoutsListView = rootView.findViewById(R.id.savedWorkouts);
            final ListView allWorkoutsListView = rootView.findViewById(R.id.allWorkouts);

            final HttpPostRequest httpPostRequest = new HttpPostRequest();
            final HttpDeleteRequest httpDeleteRequest = new HttpDeleteRequest();

            @SuppressLint("StaticFieldLeak")
            class GetSavedWorkoutsList extends AsyncTask<Void, Void, String> {
                private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
                    InputStream in = entity.getContent();

                    StringBuilder out = new StringBuilder();
                    int n = 1;
                    while (n > 0) {
                        byte[] b = new byte[4096];
                        n = in.read(b);

                        if (n > 0) out.append(new String(b, 0, n));
                    }

                    return out.toString();
                }

                @Override
                protected String doInBackground(Void... params) {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet("http://192.168.0.94:8080/userWorkouts/byUser=" + userID);
                    String text;
                    try {
                        HttpResponse response = httpClient.execute(httpGet, localContext);

                        HttpEntity entity = response.getEntity();

                        text = getASCIIContentFromEntity(entity);

                    } catch (Exception e) {
                        return e.getLocalizedMessage();
                    }

                    return text;
                }

                protected void onPostExecute(String results) {
                    if (results != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            List<UserWorkout> userWorkoutList;
                            userWorkoutList = objectMapper.readValue(results, new TypeReference<List<UserWorkout>>() {
                            });

                            List<Workout> savedWorkouts = new ArrayList<>();
                            for (UserWorkout userWorkout : userWorkoutList)
                                savedWorkouts.add(userWorkout.getWorkoutDTO());

                            final WorkoutListAdapter mWorkoutListAdapter;
                            mWorkoutListAdapter = new WorkoutListAdapter(getActivity(), (ArrayList<Workout>) savedWorkouts);
                            savedWorkoutsListView.setAdapter(mWorkoutListAdapter);

                            savedWorkoutsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    Workout clickedWorkout = mWorkoutListAdapter.getItem(i);

                                    assert clickedWorkout != null;
                                    String deleteUserWorkoutRequest = "http://192.168.0.94:8080/userWorkouts/byUserAndWorkout?userID=" + userID + "&workoutID=" + clickedWorkout.getWorkoutID();

                                    httpDeleteRequest.execute(deleteUserWorkoutRequest);
                                }
                            });

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            GetSavedWorkoutsList getSavedWorkoutsList = new GetSavedWorkoutsList();
            getSavedWorkoutsList.execute();

            @SuppressLint("StaticFieldLeak")
            class GetWorkoutsList extends AsyncTask<Void, Void, String> {
                private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
                    InputStream in = entity.getContent();

                    StringBuilder out = new StringBuilder();
                    int n = 1;
                    while (n > 0) {
                        byte[] b = new byte[4096];
                        n = in.read(b);

                        if (n > 0) out.append(new String(b, 0, n));
                    }

                    return out.toString();
                }

                @Override
                protected String doInBackground(Void... params) {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet("http://192.168.0.94:8080/workouts");
                    String text;
                    try {
                        HttpResponse response = httpClient.execute(httpGet, localContext);

                        HttpEntity entity = response.getEntity();

                        text = getASCIIContentFromEntity(entity);

                    } catch (Exception e) {
                        return e.getLocalizedMessage();
                    }

                    return text;
                }

                protected void onPostExecute(String results) {
                    if (results != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            List<Workout> workoutList;
                            workoutList = objectMapper.readValue(results, new TypeReference<List<Workout>>() {
                            });

                            final WorkoutListAdapter mWorkoutListAdapter;
                            mWorkoutListAdapter = new WorkoutListAdapter(getActivity(), (ArrayList<Workout>) workoutList);
                            allWorkoutsListView.setAdapter(mWorkoutListAdapter);

                            allWorkoutsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    Workout clickedWorkout = mWorkoutListAdapter.getItem(i);

                                    assert clickedWorkout != null;
                                    String saveWorkout = "http://192.168.0.94:8080/userWorkouts?userID=" + userID + "&workoutID=" + clickedWorkout.getWorkoutID();

                                    httpPostRequest.execute(saveWorkout);
                                }
                            });

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            GetWorkoutsList getWorkoutsList = new GetWorkoutsList();
            getWorkoutsList.execute();

            return rootView;
        }
    }

    /**
     * A placeholder fragment containing a simple meals view.
     */
    public static class MealsFragment extends Fragment {

        public MealsFragment() {

        }

        /**
         * Returns a new instance of this fragment
         */
        public static MealsFragment newInstance() {
            MealsFragment fragment = new MealsFragment();
            Bundle args = new Bundle();
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_user_landing_meals, container, false);
            final ListView savedMealsLiseView = rootView.findViewById(R.id.savedMeals);
            final ListView allMealsListView = rootView.findViewById(R.id.allMeals);

            final HttpPostRequest httpPostRequest = new HttpPostRequest();
            final HttpDeleteRequest httpDeleteRequest = new HttpDeleteRequest();

            @SuppressLint("StaticFieldLeak")
            class GetSavedMealsList extends AsyncTask<Void, Void, String> {
                private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
                    InputStream in = entity.getContent();

                    StringBuilder out = new StringBuilder();
                    int n = 1;
                    while (n > 0) {
                        byte[] b = new byte[4096];
                        n = in.read(b);


                        if (n > 0) out.append(new String(b, 0, n));
                    }

                    return out.toString();
                }

                @Override
                protected String doInBackground(Void... params) {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet("http://192.168.0.94:8080/userMeals/byUser=" + userID);
                    String text;
                    try {
                        HttpResponse response = httpClient.execute(httpGet, localContext);

                        HttpEntity entity = response.getEntity();

                        text = getASCIIContentFromEntity(entity);

                    } catch (Exception e) {
                        return e.getLocalizedMessage();
                    }

                    return text;
                }

                protected void onPostExecute(String results) {
                    if (results != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            List<UserMeal> userMealList;
                            userMealList = objectMapper.readValue(results, new TypeReference<List<UserMeal>>() {
                            });

                            List<Meal> savedMeals = new ArrayList<>();
                            for (UserMeal userMeal : userMealList)
                                savedMeals.add(userMeal.getMealDTO());

                            final MealListAdapter mMealListAdapter;
                            mMealListAdapter = new MealListAdapter(getActivity(), (ArrayList<Meal>) savedMeals);
                            savedMealsLiseView.setAdapter(mMealListAdapter);

                            savedMealsLiseView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    Meal clickedMeal = mMealListAdapter.getItem(i);

                                    assert clickedMeal != null;
                                    String deleteUserMealRequest = "http://192.168.0.94:8080/userMeals/byUserAndMeal?userID=" + userID + "&mealID=" + clickedMeal.getMealID();

                                    httpDeleteRequest.execute(deleteUserMealRequest);
                                }
                            });

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            GetSavedMealsList getSavedMealsList = new GetSavedMealsList();
            getSavedMealsList.execute();


            @SuppressLint("StaticFieldLeak")
            class GetMealsList extends AsyncTask<Void, Void, String> {
                private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
                    InputStream in = entity.getContent();

                    StringBuilder out = new StringBuilder();
                    int n = 1;
                    while (n > 0) {
                        byte[] b = new byte[4096];
                        n = in.read(b);


                        if (n > 0) out.append(new String(b, 0, n));
                    }

                    return out.toString();
                }

                @Override
                protected String doInBackground(Void... params) {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet("http://192.168.0.94:8080/meals");
                    String text;
                    try {
                        HttpResponse response = httpClient.execute(httpGet, localContext);

                        HttpEntity entity = response.getEntity();

                        text = getASCIIContentFromEntity(entity);

                    } catch (Exception e) {
                        return e.getLocalizedMessage();
                    }

                    return text;
                }

                protected void onPostExecute(String results) {
                    if (results != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            List<Meal> mealList;
                            mealList = objectMapper.readValue(results, new TypeReference<List<Meal>>() {
                            });

                            final MealListAdapter mMealListAdapter;
                            mMealListAdapter = new MealListAdapter(getActivity(), (ArrayList<Meal>) mealList);
                            allMealsListView.setAdapter(mMealListAdapter);

                            allMealsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    Meal clickedMeal = mMealListAdapter.getItem(i);

                                    assert clickedMeal != null;
                                    String saveMeal = "http://192.168.0.94:8080/userMeals?userID=" + userID + "&mealID=" + clickedMeal.getMealID();

                                    httpPostRequest.execute(saveMeal);
                                }
                            });

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            GetMealsList getMealsList = new GetMealsList();
            getMealsList.execute();

            return rootView;
        }
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);

            switch (position) {
                case 0:
                    return GoalsFragment.newInstance();
                case 1:
                    return WorkoutsFragment.newInstance();
                case 2:
                    return MealsFragment.newInstance();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
}
