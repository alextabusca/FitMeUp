package com.example.alex.fitme.models;

/**
 * Created by Alex on 5/29/2018.
 * Project: FitMe
 */
public class Workout {

    private Long workoutID;
    private String title;

    public Workout() {
    }

    public Workout(String title) {
        this.title = title;
    }

    public Long getWorkoutID() {
        return workoutID;
    }

    public void setWorkoutID(Long workoutID) {
        this.workoutID = workoutID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
