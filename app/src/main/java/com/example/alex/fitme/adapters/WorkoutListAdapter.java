package com.example.alex.fitme.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.alex.fitme.R;
import com.example.alex.fitme.models.Workout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 5/29/2018.
 * Project: FitMe
 */
public class WorkoutListAdapter extends ArrayAdapter<Workout> {

    private Context mContext;
    private List<Workout> workoutList;

    public WorkoutListAdapter(@NonNull Context context, ArrayList<Workout> list) {
        super(context, 0, list);
        mContext = context;
        workoutList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;

        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.listview_workout_item, parent, false);

        Workout currentWorkout = workoutList.get(position);

        TextView title = listItem.findViewById(R.id.workoutListTitle);
        title.setText(currentWorkout.getTitle());

        return listItem;
    }
}
