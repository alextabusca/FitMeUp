package com.example.alex.fitme.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.alex.fitme.R;
import com.example.alex.fitme.models.Goal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 5/29/2018.
 * Project: FitMe
 */
public class GoalListAdapter extends ArrayAdapter<Goal> {

    private Context mContext;
    private List<Goal> goalList;

    public GoalListAdapter(@NonNull Context context, ArrayList<Goal> list) {
        super(context, 0, list);
        mContext = context;
        goalList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;

        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.listview_goal_item, parent, false);

        Goal currentGoal = goalList.get(position);

        TextView title = listItem.findViewById(R.id.goalListTitle);
        title.setText(currentGoal.getTitle());

        TextView deadline = listItem.findViewById(R.id.goalListDeadline);
        deadline.setText(currentGoal.getDeadline().toString());

        return listItem;
    }
}
