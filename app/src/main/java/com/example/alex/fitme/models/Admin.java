package com.example.alex.fitme.models;

/**
 * Created by Alex on 5/25/2018.
 * Project: FitMe
 */
public class Admin {

    private Long adminID;
    private String name;
    private String email;
    private String password;

    public Admin() {
    }

    public Admin(Long adminID, String name, String email, String password) {
        this.adminID = adminID;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public Long getAdminID() {
        return adminID;
    }

    public void setAdminID(Long adminID) {
        this.adminID = adminID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
