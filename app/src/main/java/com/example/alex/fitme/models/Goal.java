package com.example.alex.fitme.models;

import java.sql.Date;

/**
 * Created by Alex on 5/25/2018.
 * Project: FitMe
 */
public class Goal {

    private Long goalID;
    private String title;
    private Date deadline;
    private User user;

    public Goal() {
    }

    public Goal(String title, Date deadline) {
        this.title = title;
        this.deadline = deadline;
    }

    public Goal(String title, Date deadline, User user) {
        this.title = title;
        this.deadline = deadline;
        this.user = user;
    }

    public Long getGoalID() {
        return goalID;
    }

    public void setGoalID(Long goalID) {
        this.goalID = goalID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public User getUserDTO() {
        return user;
    }

    public void setUserDTO(User user) {
        this.user = user;
    }

}
