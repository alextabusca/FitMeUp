package com.example.alex.fitme.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.alex.fitme.R;
import com.example.alex.fitme.models.Meal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 5/29/2018.
 * Project: FitMe
 */
public class MealListAdapter extends ArrayAdapter<Meal> {

    private Context mContext;
    private List<Meal> mealList;

    public MealListAdapter(@NonNull Context context, ArrayList<Meal> list) {
        super(context, 0, list);
        mContext = context;
        mealList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;

        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.listview_meal_item, parent, false);

        Meal currentMeal = mealList.get(position);

        TextView title = listItem.findViewById(R.id.mealListTitle);
        title.setText(currentMeal.getTitle());

        TextView ingredients = listItem.findViewById(R.id.mealListIngredients);
        ingredients.setText(currentMeal.getIngredients());

        TextView preparation = listItem.findViewById(R.id.mealListPreparation);
        preparation.setText(currentMeal.getPreparation());


        return listItem;
    }
}
