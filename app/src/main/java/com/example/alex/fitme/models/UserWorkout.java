package com.example.alex.fitme.models;

/**
 * Created by Alex on 6/1/2018.
 * Project: FitMe
 */
public class UserWorkout {

    private Long userWorkoutID;
    private User user;
    private Workout workout;

    public UserWorkout() {
    }

    public UserWorkout(User user, Workout workout) {
        this.user = user;
        this.workout = workout;
    }

    public UserWorkout(Long userWorkoutID, User user, Workout workout) {
        this.userWorkoutID = userWorkoutID;
        this.user = user;
        this.workout = workout;
    }

    public Long getUserWorkoutID() {
        return userWorkoutID;
    }

    public void setUserWorkoutID(Long userWorkoutID) {
        this.userWorkoutID = userWorkoutID;
    }

    public User getUserDTO() {
        return user;
    }

    public void setUserDTO(User user) {
        this.user = user;
    }

    public Workout getWorkoutDTO() {
        return workout;
    }

    public void setWorkoutDTO(Workout workout) {
        this.workout = workout;
    }
}
