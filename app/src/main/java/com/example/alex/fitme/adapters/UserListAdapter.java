package com.example.alex.fitme.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.alex.fitme.R;
import com.example.alex.fitme.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 5/29/2018.
 * Project: FitMe
 */
public class UserListAdapter extends ArrayAdapter<User> {

    private Context mContext;
    private List<User> userList;

    public UserListAdapter(@NonNull Context context, ArrayList<User> list) {
        super(context, 0, list);
        mContext = context;
        userList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;

        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.listview_user_item, parent, false);

        User currentUser = userList.get(position);

        TextView id = listItem.findViewById(R.id.userListItemId);
        id.setText(currentUser.getUserID().toString());

        TextView name = listItem.findViewById(R.id.userListItemName);
        name.setText(currentUser.getName());

        TextView email = listItem.findViewById(R.id.userListItemEmail);
        email.setText(currentUser.getEmail());

        TextView password = listItem.findViewById(R.id.userListItemPassword);
        password.setText(currentUser.getPassword());

        return listItem;
    }


}
