package com.example.alex.fitme.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.example.alex.fitme.R;
import com.example.alex.fitme.adapters.GoalListAdapter;
import com.example.alex.fitme.adapters.MealListAdapter;
import com.example.alex.fitme.adapters.UserListAdapter;
import com.example.alex.fitme.adapters.WorkoutListAdapter;
import com.example.alex.fitme.models.Goal;
import com.example.alex.fitme.models.Meal;
import com.example.alex.fitme.models.User;
import com.example.alex.fitme.models.Workout;
import com.example.alex.fitme.responses.HttpDeleteRequest;
import com.example.alex.fitme.responses.HttpPostRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class AdminLanding extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_landing);

        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_admin_landing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A placeholder fragment containing a simple user view.
     */
    public static class UsersFragment extends Fragment {

        public UsersFragment() {

        }

        /**
         * Returns a new instance of this fragment
         */
        public static UsersFragment newInstance() {
            UsersFragment fragment = new UsersFragment();
            Bundle args = new Bundle();
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_admin_landing_users, container, false);
            final ListView userListView = rootView.findViewById(R.id.adminUsersList);
            //final TextView textView = rootView.findViewById(R.id.adminUsersText);

            //textView.setText("FragmentWorks");

            final HttpPostRequest httpPostRequest = new HttpPostRequest();
            final HttpDeleteRequest httpDeleteRequest = new HttpDeleteRequest();

            FloatingActionButton adminAddUser = rootView.findViewById(R.id.adminAddUser);
            adminAddUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // inflate alert dialog xml
                    LayoutInflater li = LayoutInflater.from(getContext());
                    View dialogView = li.inflate(R.layout.custom_dialog_create_user, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());

                    // set title
                    alertDialogBuilder.setTitle("Add user");

                    // set custom dialog icon
                    //alertDialogBuilder.setIcon(R.drawable.ic_launcher);

                    // set custom_dialog_create_user_create_user.xml to alertDialog builder
                    alertDialogBuilder.setView(dialogView);

                    final EditText nameInput = dialogView.findViewById(R.id.dialog_name_input);
                    final EditText emailInput = dialogView.findViewById(R.id.dialog_email_input);
                    final EditText passwordInput = dialogView.findViewById(R.id.dialog_password_input);

                    // set dialog message
                    alertDialogBuilder
                            .setCancelable(false)
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // get user input and set it to etOutput
                                            // edit text
                                            //textView.append(nameInput.getText());
                                            //textView.append(emailInput.getText());
                                            //textView.append(passwordInput.getText());

                                            String httpPost = "http://192.168.0.94:8080/users?name=" + nameInput.getText() + "&email=" + emailInput.getText() + "&password=" + passwordInput.getText();

                                            httpPostRequest.execute(httpPost);
                                        }
                                    })
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            });


            @SuppressLint("StaticFieldLeak")
            class GetUsersList extends AsyncTask<Void, Void, String> {
                private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
                    InputStream in = entity.getContent();

                    StringBuilder out = new StringBuilder();
                    int n = 1;
                    while (n > 0) {
                        byte[] b = new byte[4096];
                        n = in.read(b);

                        if (n > 0) out.append(new String(b, 0, n));
                    }

                    return out.toString();
                }

                @Override
                protected String doInBackground(Void... params) {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet("http://192.168.0.94:8080/users");
                    String text;
                    try {
                        HttpResponse response = httpClient.execute(httpGet, localContext);

                        HttpEntity entity = response.getEntity();

                        text = getASCIIContentFromEntity(entity);

                    } catch (Exception e) {
                        return e.getLocalizedMessage();
                    }

                    return text;
                }

                protected void onPostExecute(String results) {
                    if (results != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            List<User> userList;
                            userList = objectMapper.readValue(results, new TypeReference<List<User>>() {
                            });

                            final UserListAdapter mUserListAdapter;
                            mUserListAdapter = new UserListAdapter(getActivity(), (ArrayList<User>) userList);
                            userListView.setAdapter(mUserListAdapter);

                            userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    User clickedUser = mUserListAdapter.getItem(i);

                                    assert clickedUser != null;
                                    String deleteUserRequest = "http://192.168.0.94:8080/users?userID=" + clickedUser.getUserID();

                                    httpDeleteRequest.execute(deleteUserRequest);
                                }
                            });

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            GetUsersList getUsersList = new GetUsersList();
            getUsersList.execute();

            return rootView;
        }
    }

    /**
     * A placeholder fragment containing a simple goals view.
     */
    public static class GoalsFragment extends Fragment {

        public GoalsFragment() {

        }

        /**
         * Returns a new instance of this fragment
         */
        public static GoalsFragment newInstance() {
            GoalsFragment fragment = new GoalsFragment();
            Bundle args = new Bundle();
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_admin_landing_goals, container, false);
            final ListView goalsListView = rootView.findViewById(R.id.adminGoalsList);

            @SuppressLint("StaticFieldLeak")
            class GetGoalsList extends AsyncTask<Void, Void, String> {
                private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
                    InputStream in = entity.getContent();

                    StringBuilder out = new StringBuilder();
                    int n = 1;
                    while (n > 0) {
                        byte[] b = new byte[4096];
                        n = in.read(b);


                        if (n > 0) out.append(new String(b, 0, n));
                    }

                    return out.toString();
                }

                @Override
                protected String doInBackground(Void... params) {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet("http://192.168.0.94:8080/goals");
                    String text;
                    try {
                        HttpResponse response = httpClient.execute(httpGet, localContext);

                        HttpEntity entity = response.getEntity();

                        text = getASCIIContentFromEntity(entity);

                    } catch (Exception e) {
                        return e.getLocalizedMessage();
                    }

                    return text;
                }

                protected void onPostExecute(String results) {
                    if (results != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            List<Goal> goalList;
                            goalList = objectMapper.readValue(results, new TypeReference<List<Goal>>() {
                            });

                            GoalListAdapter mGoalListAdapter;

                            mGoalListAdapter = new GoalListAdapter(getActivity(), (ArrayList<Goal>) goalList);

                            goalsListView.setAdapter(mGoalListAdapter);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            GetGoalsList getGoalsList = new GetGoalsList();
            getGoalsList.execute();

            return rootView;
        }
    }

    /**
     * A placeholder fragment containing a simple workouts view.
     */
    public static class WorkoutsFragment extends Fragment {

        public WorkoutsFragment() {

        }

        /**
         * Returns a new instance of this fragment
         */
        public static WorkoutsFragment newInstance() {
            WorkoutsFragment fragment = new WorkoutsFragment();
            Bundle args = new Bundle();
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_admin_landing_workouts, container, false);
            final ListView workoutsListView = rootView.findViewById(R.id.adminWorkoutsList);

            final HttpPostRequest httpPostRequest = new HttpPostRequest();
            final HttpDeleteRequest httpDeleteRequest = new HttpDeleteRequest();

            @SuppressLint("StaticFieldLeak")
            class GetWorkoutsList extends AsyncTask<Void, Void, String> {
                private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
                    InputStream in = entity.getContent();

                    StringBuilder out = new StringBuilder();
                    int n = 1;
                    while (n > 0) {
                        byte[] b = new byte[4096];
                        n = in.read(b);

                        if (n > 0) out.append(new String(b, 0, n));
                    }

                    return out.toString();
                }

                @Override
                protected String doInBackground(Void... params) {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet("http://192.168.0.94:8080/workouts");
                    String text;
                    try {
                        HttpResponse response = httpClient.execute(httpGet, localContext);

                        HttpEntity entity = response.getEntity();

                        text = getASCIIContentFromEntity(entity);

                    } catch (Exception e) {
                        return e.getLocalizedMessage();
                    }

                    return text;
                }

                protected void onPostExecute(String results) {
                    if (results != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            List<Workout> workoutList;
                            workoutList = objectMapper.readValue(results, new TypeReference<List<Workout>>() {
                            });

                            final WorkoutListAdapter mWorkoutListAdapter;
                            mWorkoutListAdapter = new WorkoutListAdapter(getActivity(), (ArrayList<Workout>) workoutList);
                            workoutsListView.setAdapter(mWorkoutListAdapter);

                            workoutsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    Workout clickedWorkout = mWorkoutListAdapter.getItem(i);

                                    assert clickedWorkout != null;
                                    String deleteWorkoutRequest = "http://192.168.0.94:8080/workouts?workoutID=" + clickedWorkout.getWorkoutID();

                                    httpDeleteRequest.execute(deleteWorkoutRequest);
                                }
                            });

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            FloatingActionButton adminAddUser = rootView.findViewById(R.id.adminAddWorkout);
            adminAddUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // inflate alert dialog xml
                    LayoutInflater li = LayoutInflater.from(getContext());
                    View dialogView = li.inflate(R.layout.custom_dialog_create_workout, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());

                    // set title
                    alertDialogBuilder.setTitle("Add workout");

                    // set custom dialog icon
                    //alertDialogBuilder.setIcon(R.drawable.ic_launcher);

                    // set custom_dialog_create_user.xml to alertDialog builder
                    alertDialogBuilder.setView(dialogView);

                    final EditText titleInput = dialogView.findViewById(R.id.dialog_workoutTitle_input);

                    // set dialog message
                    alertDialogBuilder
                            .setCancelable(false)
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // get user input and set it to etOutput
                                            // edit text
                                            //textView.append(nameInput.getText());
                                            //textView.append(emailInput.getText());
                                            //textView.append(passwordInput.getText());

                                            String httpPost = "http://192.168.0.94:8080/workouts?title=" + titleInput.getText();

                                            httpPostRequest.execute(httpPost);
                                        }
                                    })
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            });

            GetWorkoutsList getWorkoutsList = new GetWorkoutsList();
            getWorkoutsList.execute();

            return rootView;
        }
    }

    /**
     * A placeholder fragment containing a simple meals view.
     */
    public static class MealsFragment extends Fragment {

        public MealsFragment() {

        }

        /**
         * Returns a new instance of this fragment
         */
        public static MealsFragment newInstance() {
            MealsFragment fragment = new MealsFragment();
            Bundle args = new Bundle();
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_admin_landing_meals, container, false);
            final ListView mealListView = rootView.findViewById(R.id.adminMealsList);

            final HttpPostRequest httpPostRequest = new HttpPostRequest();
            final HttpDeleteRequest httpDeleteRequest = new HttpDeleteRequest();

            @SuppressLint("StaticFieldLeak")
            class GetMealsList extends AsyncTask<Void, Void, String> {
                private String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
                    InputStream in = entity.getContent();

                    StringBuilder out = new StringBuilder();
                    int n = 1;
                    while (n > 0) {
                        byte[] b = new byte[4096];
                        n = in.read(b);


                        if (n > 0) out.append(new String(b, 0, n));
                    }

                    return out.toString();
                }

                @Override
                protected String doInBackground(Void... params) {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet("http://192.168.0.94:8080/meals");
                    String text;
                    try {
                        HttpResponse response = httpClient.execute(httpGet, localContext);

                        HttpEntity entity = response.getEntity();

                        text = getASCIIContentFromEntity(entity);

                    } catch (Exception e) {
                        return e.getLocalizedMessage();
                    }

                    return text;
                }

                protected void onPostExecute(String results) {
                    if (results != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            List<Meal> mealList;
                            mealList = objectMapper.readValue(results, new TypeReference<List<Meal>>() {
                            });

                            final MealListAdapter mMealListAdapter;
                            mMealListAdapter = new MealListAdapter(getActivity(), (ArrayList<Meal>) mealList);
                            mealListView.setAdapter(mMealListAdapter);

                            mealListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    Meal clickedMeal = mMealListAdapter.getItem(i);

                                    assert clickedMeal != null;
                                    String deleteMealRequest = "http://192.168.0.94:8080/meals?mealID=" + clickedMeal.getMealID();

                                    httpDeleteRequest.execute(deleteMealRequest);
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            FloatingActionButton adminAddMeal = rootView.findViewById(R.id.adminAddMeal);
            adminAddMeal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // inflate alert dialog xml
                    LayoutInflater li = LayoutInflater.from(getContext());
                    View dialogView = li.inflate(R.layout.custom_dialog_create_meal, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());

                    // set title
                    alertDialogBuilder.setTitle("Add meal");

                    // set custom_dialog_create_meal.xml to alertDialog builder
                    alertDialogBuilder.setView(dialogView);

                    final EditText titleInput = dialogView.findViewById(R.id.dialog_mealTitle_input);
                    final EditText ingredientsInput = dialogView.findViewById(R.id.dialog_mealIngredients_input);
                    final EditText preparationInput = dialogView.findViewById(R.id.dialog_mealPreparation_input);

                    // set dialog message
                    alertDialogBuilder
                            .setCancelable(false)
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            String httpPost = "http://192.168.0.94:8080/meals?title=" + titleInput.getText() + "&ingredients=" + ingredientsInput.getText() + "&preparation=" + preparationInput.getText();

                                            httpPostRequest.execute(httpPost);
                                        }
                                    })
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            });

            GetMealsList getMealsList = new GetMealsList();
            getMealsList.execute();

            return rootView;
        }
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            /*if (position == 0)
                return UsersFragment.newInstance();

            return PlaceholderFragment.newInstance(position + 1);*/
            switch (position) {
                case 0:
                    return UsersFragment.newInstance();
                case 1:
                    return GoalsFragment.newInstance();
                case 2:
                    return WorkoutsFragment.newInstance();
                case 3:
                    return MealsFragment.newInstance();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }
    }
}
