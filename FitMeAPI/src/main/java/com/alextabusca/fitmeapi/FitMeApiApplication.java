package com.alextabusca.fitmeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class FitMeApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(FitMeApiApplication.class, args);
    }
}
