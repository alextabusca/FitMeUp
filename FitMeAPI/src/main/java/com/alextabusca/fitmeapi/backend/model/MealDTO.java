package com.alextabusca.fitmeapi.backend.model;

import java.io.Serializable;

public class MealDTO implements Serializable {

    private Long mealID;
    private String title;
    private String ingredients;
    private String preparation;

    public MealDTO() {
    }

    public MealDTO(String title, String ingredients, String preparation) {
        this.title = title;
        this.ingredients = ingredients;
        this.preparation = preparation;
    }


    public Long getMealID() {
        return mealID;
    }

    public void setMealID(Long mealID) {
        this.mealID = mealID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

}
