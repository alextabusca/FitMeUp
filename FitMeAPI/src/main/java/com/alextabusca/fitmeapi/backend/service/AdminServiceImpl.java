package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.AdminModel;
import com.alextabusca.fitmeapi.backend.dataAccess.repository.AdminRepository;
import com.alextabusca.fitmeapi.backend.model.AdminDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {

    private final AdminRepository adminRepository;

    @Autowired
    public AdminServiceImpl(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    @Override
    public List<AdminDTO> getAllAdmins() {
        try {
            return modelListToDTO(adminRepository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public AdminDTO getAdminByID(Long adminID) {
        try {
            AdminModel adminModel = adminRepository.getOne(adminID);

            return modelToDTO(adminModel);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public AdminDTO loginAdmin(String email, String password) {
        try {
            AdminModel adminModel = adminRepository.findAdminModelByEmailAndPassword(email, password);

            return modelToDTO(adminModel);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public AdminDTO saveAdmin(AdminDTO adminDTO) {
        AdminModel adminToBoSaved = new AdminModel(adminDTO.getName(), adminDTO.getEmail(), adminDTO.getPassword());

        try {
            return modelToDTO(adminRepository.save(adminToBoSaved));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public AdminDTO updateAdmin(Long adminID, AdminDTO adminDTO) {
        AdminModel adminToBeUpdated = adminRepository.getOne(adminID);

        if (adminDTO.getName() != null)
            adminToBeUpdated.setName(adminDTO.getName());

        if (adminDTO.getEmail() != null)
            adminToBeUpdated.setEmail(adminDTO.getEmail());

        if (adminDTO.getPassword() != null)
            adminToBeUpdated.setPassword(adminDTO.getPassword());

        try {
            return modelToDTO(adminRepository.save(adminToBeUpdated));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteAdminByID(Long adminID) {
        adminRepository.deleteById(adminID);
    }

    private AdminDTO modelToDTO(AdminModel adminModel) {
        AdminDTO adminDTO = new AdminDTO(adminModel.getName(), adminModel.getEmail(), adminModel.getPassword());
        adminDTO.setAdminID(adminModel.getAdminID());

        return adminDTO;
    }

    private List<AdminDTO> modelListToDTO(List<AdminModel> adminModelList) {
        List<AdminDTO> adminDTOList = new ArrayList<>();

        for (AdminModel adminModel : adminModelList)
            adminDTOList.add(modelToDTO(adminModel));

        return adminDTOList;
    }
}
