package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.MealModel;
import com.alextabusca.fitmeapi.backend.dataAccess.repository.MealRepository;
import com.alextabusca.fitmeapi.backend.model.MealDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MealServiceImpl implements MealService {

    private final MealRepository mealRepository;

    public MealServiceImpl(MealRepository mealRepository) {
        this.mealRepository = mealRepository;
    }

    @Override
    public List<MealDTO> getAllMeals() {
        try {
            return modelListToDTOList(mealRepository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public MealDTO getMealByID(Long mealID) {
        MealModel mealModel = mealRepository.getOne(mealID);

        try {
            return modelToDTO(mealModel);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public MealDTO saveMeal(MealDTO mealDTO) {
        MealModel mealModel = new MealModel(mealDTO.getTitle(), mealDTO.getIngredients(), mealDTO.getPreparation());

        try {
            return modelToDTO(mealRepository.save(mealModel));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public MealDTO updateMeal(Long mealID, MealDTO mealDTO) {
        MealModel mealModel = new MealModel(mealDTO.getTitle(), mealDTO.getIngredients(), mealDTO.getPreparation());

        if (mealDTO.getTitle() != null)
            mealModel.setTitle(mealDTO.getTitle());

        if (mealDTO.getIngredients() != null)
            mealModel.setIngredients(mealDTO.getIngredients());

        if (mealDTO.getPreparation() != null)
            mealModel.setPreparation(mealDTO.getPreparation());

        try {
            return modelToDTO(mealRepository.save(mealModel));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteMealByID(Long mealID) {
        mealRepository.deleteById(mealID);
    }

    private MealDTO modelToDTO(MealModel mealModel) {
        MealDTO mealDTO = new MealDTO(mealModel.getTitle(), mealModel.getIngredients(), mealModel.getPreparation());
        mealDTO.setMealID(mealModel.getMealID());

        return mealDTO;
    }

    private List<MealDTO> modelListToDTOList(List<MealModel> mealModelList) {
        List<MealDTO> mealDTOList = new ArrayList<>();

        for (MealModel mealModel : mealModelList)
            mealDTOList.add(modelToDTO(mealModel));

        return mealDTOList;
    }
}
