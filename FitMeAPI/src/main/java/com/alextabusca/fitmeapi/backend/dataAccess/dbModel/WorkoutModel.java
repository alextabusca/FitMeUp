package com.alextabusca.fitmeapi.backend.dataAccess.dbModel;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Set;

@Entity
@Proxy(lazy = false)
@Table(name = "workout")
public class WorkoutModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "workout_id")
    private Long workoutID;

    @Column(name = "title")
    private String title;

    @JsonIgnore
    @JsonBackReference
    @ManyToMany(mappedBy = "userUserWorkout", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<UserModel> workoutUserWorkout;

    public WorkoutModel() {
    }

    public WorkoutModel(String title) {
        this.title = title;
    }

    public Long getWorkoutID() {
        return workoutID;
    }

    public void setWorkoutID(Long workoutID) {
        this.workoutID = workoutID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<UserModel> getWorkoutUserWorkout() {
        return workoutUserWorkout;
    }

    public void setWorkoutUserWorkout(Set<UserModel> workoutUserWorkout) {
        this.workoutUserWorkout = workoutUserWorkout;
    }
}
