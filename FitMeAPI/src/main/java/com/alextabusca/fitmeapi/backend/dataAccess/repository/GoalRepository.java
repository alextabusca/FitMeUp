package com.alextabusca.fitmeapi.backend.dataAccess.repository;

import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.GoalModel;
import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface GoalRepository extends JpaRepository<GoalModel, Long> {

    List<GoalModel> getGoalModelsByUserModel(UserModel userModel);
}
