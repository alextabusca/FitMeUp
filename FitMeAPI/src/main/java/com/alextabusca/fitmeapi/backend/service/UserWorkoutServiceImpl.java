package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.UserModel;
import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.UserWorkoutModel;
import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.WorkoutModel;
import com.alextabusca.fitmeapi.backend.dataAccess.repository.UserRepository;
import com.alextabusca.fitmeapi.backend.dataAccess.repository.UserWorkoutRepository;
import com.alextabusca.fitmeapi.backend.dataAccess.repository.WorkoutRepository;
import com.alextabusca.fitmeapi.backend.model.UserDTO;
import com.alextabusca.fitmeapi.backend.model.UserWorkoutDTO;
import com.alextabusca.fitmeapi.backend.model.WorkoutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserWorkoutServiceImpl implements UserWorkoutService {

    private final UserWorkoutRepository userWorkoutRepository;
    private final UserRepository userRepository;
    private final WorkoutRepository workoutRepository;

    @Autowired
    public UserWorkoutServiceImpl(UserWorkoutRepository userWorkoutRepository, UserRepository userRepository, WorkoutRepository workoutRepository) {
        this.userWorkoutRepository = userWorkoutRepository;
        this.userRepository = userRepository;
        this.workoutRepository = workoutRepository;
    }

    @Override
    public List<UserWorkoutDTO> getAllUserWorkouts() {
        try {
            return modelListToDTOList(userWorkoutRepository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserWorkoutDTO getUserWorkoutByID(Long userWorkoutID) {
        try {
            UserWorkoutModel userWorkoutModel = userWorkoutRepository.getOne(userWorkoutID);

            return modelToDTO(userWorkoutModel);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<UserWorkoutDTO> getUserWorkoutByUserID(Long userID) {
        try {
            List<UserWorkoutModel> userList;

            UserModel userModel = userRepository.getOne(userID);
            userList = userWorkoutRepository.findUserWorkoutModelByUserModel(userModel);

            return modelListToDTOList(userList);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<UserWorkoutDTO> getUserWorkoutByWorkoutID(Long workoutID) {
        try {
            List<UserWorkoutModel> workoutList;

            WorkoutModel workoutModel = workoutRepository.getOne(workoutID);
            workoutList = userWorkoutRepository.findUserWorkoutModelByWorkoutModel(workoutModel);

            return modelListToDTOList(workoutList);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserWorkoutDTO saveUserWorkout(Long userID, Long workoutID) {
        try {
            UserModel userModel = userRepository.getOne(userID);
            WorkoutModel workoutModel = workoutRepository.getOne(workoutID);

            userModel.getUserUserWorkout().add(workoutModel);
            workoutModel.getWorkoutUserWorkout().add(userModel);

            UserWorkoutModel userWorkoutModel = new UserWorkoutModel(userModel, workoutModel);

            return modelToDTO(userWorkoutRepository.save(userWorkoutModel));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserWorkoutDTO updateUserWorkout(Long userWorkoutID, Long userID, Long workoutID) {
        try {
            UserWorkoutModel userWorkoutModel = userWorkoutRepository.getOne(userWorkoutID);
            UserModel userModel = userRepository.getOne(userID);
            WorkoutModel workoutModel = workoutRepository.getOne(workoutID);

            userWorkoutModel.setUserModel(userModel);
            userWorkoutModel.setWorkoutModel(workoutModel);

            return modelToDTO(userWorkoutRepository.save(userWorkoutModel));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteUserWorkoutByID(Long userWorkoutID) {
        try {
            userWorkoutRepository.deleteById(userWorkoutID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteUserWorkoutByUserIDAndWorkoutID(Long userID, Long workoutID) {
        try {
            UserModel userModel = userRepository.getOne(userID);
            WorkoutModel workoutModel = workoutRepository.getOne(workoutID);

            userWorkoutRepository.deleteUserWorkoutModelByUserModelAndWorkoutModel(userModel, workoutModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private UserWorkoutDTO modelToDTO(UserWorkoutModel userWorkoutModel) {
        UserWorkoutDTO userWorkoutDTO = new UserWorkoutDTO();

        UserModel userModel = userWorkoutModel.getUserModel();
        WorkoutModel workoutModel = userWorkoutModel.getWorkoutModel();

        UserDTO userDTO = new UserDTO(userModel.getName(), userModel.getEmail(), userModel.getPassword());
        userDTO.setUserID(userModel.getUserID());

        WorkoutDTO workoutDTO = new WorkoutDTO(workoutModel.getTitle());
        workoutDTO.setWorkoutID(workoutModel.getWorkoutID());

        userWorkoutDTO.setUserDTO(userDTO);
        userWorkoutDTO.setWorkoutDTO(workoutDTO);
        userWorkoutDTO.setUserWorkoutID(userWorkoutModel.getUserWorkoutID());

        return userWorkoutDTO;
    }


    private List<UserWorkoutDTO> modelListToDTOList(List<UserWorkoutModel> userWorkoutModelList) {
        List<UserWorkoutDTO> userWorkoutDTOS = new ArrayList<>();

        for (UserWorkoutModel userWorkoutModel : userWorkoutModelList)
            userWorkoutDTOS.add(modelToDTO(userWorkoutModel));

        return userWorkoutDTOS;
    }
}
