package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.model.MealDTO;

import java.util.List;

public interface MealService {

    List<MealDTO> getAllMeals();

    MealDTO getMealByID(Long mealID);

    MealDTO saveMeal(MealDTO mealDTO);

    MealDTO updateMeal(Long mealID, MealDTO mealDTO);

    void deleteMealByID(Long mealID);

}
