package com.alextabusca.fitmeapi.backend.model;

import java.io.Serializable;

public class UserWorkoutDTO implements Serializable {

    private Long userWorkoutID;
    private UserDTO userDTO;
    private WorkoutDTO workoutDTO;

    public UserWorkoutDTO() {
    }

    public UserWorkoutDTO(UserDTO userDTO, WorkoutDTO workoutDTO) {
        this.userDTO = userDTO;
        this.workoutDTO = workoutDTO;
    }

    public UserWorkoutDTO(Long userWorkoutID, UserDTO userDTO, WorkoutDTO workoutDTO) {
        this.userWorkoutID = userWorkoutID;
        this.userDTO = userDTO;
        this.workoutDTO = workoutDTO;
    }

    public Long getUserWorkoutID() {
        return userWorkoutID;
    }

    public void setUserWorkoutID(Long userWorkoutID) {
        this.userWorkoutID = userWorkoutID;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public WorkoutDTO getWorkoutDTO() {
        return workoutDTO;
    }

    public void setWorkoutDTO(WorkoutDTO workoutDTO) {
        this.workoutDTO = workoutDTO;
    }
}
