package com.alextabusca.fitmeapi.backend.dataAccess.repository;

import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.MealModel;
import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.UserMealModel;
import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface UserMealRepository extends JpaRepository<UserMealModel, Long> {

    List<UserMealModel> findUserMealModelByUserModel(UserModel userModel);

    List<UserMealModel> findUserMealModelByMealModel(MealModel mealModel);

    void deleteUserMealModelByUserModelAndMealModel(UserModel userModel, MealModel mealModel);
}
