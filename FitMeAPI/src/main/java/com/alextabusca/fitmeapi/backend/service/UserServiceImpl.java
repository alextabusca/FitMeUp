package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.UserModel;
import com.alextabusca.fitmeapi.backend.dataAccess.repository.UserRepository;
import com.alextabusca.fitmeapi.backend.model.UserDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<UserDTO> getAllUsers() {
        try {
            return modelListToDTOList(userRepository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserDTO getUserByID(Long userID) {
        try {
            UserModel userModel = userRepository.getOne(userID);

            return modelToDTO(userModel);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserDTO loginUser(String email, String password) {
        try {
            UserModel userModel = userRepository.findByEmailAndPassword(email, password);

            return modelToDTO(userModel);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserDTO saveUser(UserDTO userDTO) {
        UserModel userToBeSaved = new UserModel(userDTO.getName(), userDTO.getEmail(), userDTO.getPassword());

        try {
            return modelToDTO(userRepository.save(userToBeSaved));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserDTO updateUser(Long userID, UserDTO userDTO) {
        UserModel userToBeUpdated = userRepository.getOne(userID);

        if (userDTO.getName() != null)
            userToBeUpdated.setName(userDTO.getName());

        if (userDTO.getEmail() != null)
            userToBeUpdated.setEmail(userDTO.getEmail());

        if (userDTO.getPassword() != null)
            userToBeUpdated.setPassword(userDTO.getPassword());

        try {
            return modelToDTO(userRepository.save(userToBeUpdated));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteUserByID(Long userID) {
        userRepository.deleteById(userID);
    }

    private UserDTO modelToDTO(UserModel userModel) {
        UserDTO userDTO = new UserDTO(userModel.getName(), userModel.getEmail(), userModel.getPassword());
        userDTO.setUserID(userModel.getUserID());

        return userDTO;
    }

    private List<UserDTO> modelListToDTOList(List<UserModel> userModelList) {
        List<UserDTO> userDTOList = new ArrayList<>();

        for (UserModel userModel : userModelList)
            userDTOList.add(modelToDTO(userModel));

        return userDTOList;
    }
}
