package com.alextabusca.fitmeapi.backend.controller;

import com.alextabusca.fitmeapi.backend.model.MealDTO;
import com.alextabusca.fitmeapi.backend.service.MealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/meals")
public class MealController {

    private final MealService mealService;

    @Autowired
    public MealController(MealService mealService) {
        this.mealService = mealService;
    }

    @GetMapping()
    public List<MealDTO> getAllMeals() {
        try {
            return mealService.getAllMeals();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/{mealID}")
    public MealDTO getMealByID(@PathVariable Long mealID) {
        try {
            return mealService.getMealByID(mealID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping
    public MealDTO saveMeal(@RequestParam String title, @RequestParam String ingredients, @RequestParam String preparation) {
        try {
            MealDTO mealDTO = new MealDTO(title, ingredients, preparation);
            return mealService.saveMeal(mealDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PutMapping
    public MealDTO updateMeal(@RequestParam Long mealID, @RequestParam String title, @RequestParam String ingredients, @RequestParam String preparation) {
        try {
            MealDTO mealDTO = new MealDTO(title, ingredients, preparation);
            return mealService.updateMeal(mealID, mealDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DeleteMapping
    public String deleteMealByID(@RequestParam Long mealID) {
        try {
            mealService.deleteMealByID(mealID);
            return "Meal with id = " + mealID + " has been successfully deleted";
        } catch (Exception e) {
            e.printStackTrace();
            return "meal with id = " + mealID + " could not be deleted";
        }
    }
}
