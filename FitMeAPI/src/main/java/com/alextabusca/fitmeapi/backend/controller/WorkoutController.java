package com.alextabusca.fitmeapi.backend.controller;

import com.alextabusca.fitmeapi.backend.model.WorkoutDTO;
import com.alextabusca.fitmeapi.backend.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/workouts")
public class WorkoutController {

    private final WorkoutService workoutService;

    @Autowired
    public WorkoutController(WorkoutService workoutService) {
        this.workoutService = workoutService;
    }

    @GetMapping()
    public List<WorkoutDTO> getAllWorkouts() {
        try {
            return workoutService.getAllWorkouts();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/{workoutID}")
    public WorkoutDTO getWorkoutByID(@PathVariable Long workoutID) {
        try {
            return workoutService.getWorkoutByID(workoutID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping()
    public WorkoutDTO saveWorkout(@RequestParam String title) {
        try {
            WorkoutDTO workoutDTO = new WorkoutDTO(title);

            return workoutService.saveWorkout(workoutDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PutMapping()
    public WorkoutDTO updateWorkout(@RequestParam Long workoutID, @RequestParam String title) {
        try {
            WorkoutDTO workoutDTO = new WorkoutDTO(title);

            return workoutService.updateWorkout(workoutID, workoutDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DeleteMapping
    public String deleteWorkoutByID(@RequestParam Long workoutID) {
        try {
            workoutService.deleteWorkoutByID(workoutID);
            return "Workout with id = " + workoutID + " has been successfully deleted";
        } catch (Exception e) {
            e.printStackTrace();
            return "Workout with id = " + workoutID + " could not be deleted";
        }
    }


}
