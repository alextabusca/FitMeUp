package com.alextabusca.fitmeapi.backend.controller;

import com.alextabusca.fitmeapi.backend.model.UserMealDTO;
import com.alextabusca.fitmeapi.backend.service.UserMealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/userMeals")
public class UserMealController {

    private final UserMealService userMealService;

    @Autowired
    public UserMealController(UserMealService userMealService) {
        this.userMealService = userMealService;
    }

    @GetMapping()
    public List<UserMealDTO> getAllUserMeals() {
        try {
            return userMealService.getAllUserMeals();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/{userMealID}")
    public UserMealDTO getUserMealByID(@PathVariable Long userMealID) {
        try {
            return userMealService.getUserMealByID(userMealID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/byUser={userID}")
    public List<UserMealDTO> getUserMealByUserID(@PathVariable Long userID) {
        try {
            return userMealService.getUserMealByUserID(userID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/byMeal={mealID}")
    public List<UserMealDTO> getUserMealByMealID(@PathVariable Long mealID) {
        try {
            return userMealService.getUserMealByMealID(mealID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping()
    public UserMealDTO saveUserMeal(@RequestParam Long userID, @RequestParam Long mealID) {
        try {
            return userMealService.saveUserMeal(userID, mealID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PutMapping()
    public UserMealDTO updateUserMeal(@RequestParam Long userMealID, @RequestParam Long userID, @RequestParam Long mealID) {
        try {
            return userMealService.updateUserMeal(userMealID, userID, mealID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DeleteMapping()
    public String deleteUserMealByID(@RequestParam Long userMealID) {
        try {
            userMealService.deleteUserMealByID(userMealID);
            return "UserMeal with id = " + userMealID + "has been successfully deleted";
        } catch (Exception e) {
            e.printStackTrace();
            return "UserMeal with id = " + userMealID + "could not be deleted";
        }
    }

    @DeleteMapping("/byUserAndMeal")
    public String deleteUserMealByUserIDAndMealID(@RequestParam Long userID, @RequestParam Long mealID) {
        try {
            userMealService.deleteUserMealByUserIDAndMealID(userID, mealID);
            return "UserMeal has been successfully deleted";
        } catch (Exception e) {
            e.printStackTrace();
            return "UserMeal could not be deleted";
        }
    }
}
