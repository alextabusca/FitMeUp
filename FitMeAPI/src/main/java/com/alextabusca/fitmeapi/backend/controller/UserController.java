package com.alextabusca.fitmeapi.backend.controller;

import com.alextabusca.fitmeapi.backend.model.UserDTO;
import com.alextabusca.fitmeapi.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public List<UserDTO> getAllUsers() {
        try {
            return userService.getAllUsers();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/{userID}")
    public UserDTO getUserByID(@PathVariable Long userID) {
        try {
            return userService.getUserByID(userID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/{email}/{password}")
    public UserDTO loginUser(@PathVariable String email, @PathVariable String password) {
        try {
            return userService.loginUser(email, password);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping()
    public UserDTO saveUser(@RequestParam String name, @RequestParam String email, @RequestParam String password) {
        try {
            UserDTO userDTO = new UserDTO(name, email, password);
            return userService.saveUser(userDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PutMapping()
    public UserDTO updateUser(@RequestParam Long userID, @RequestParam String name, @RequestParam String email, @RequestParam String password) {
        try {
            UserDTO userDTO = new UserDTO(name, email, password);
            return userService.updateUser(userID, userDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DeleteMapping()
    public String deleteUserByID(@RequestParam Long userID) {

        try {
            userService.deleteUserByID(userID);
            return "User with id = " + userID + " has been successfully deleted";
        } catch (Exception e) {
            e.printStackTrace();
            return "User with id = " + userID + " could not be deleted";
        }
    }

}
