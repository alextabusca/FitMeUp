package com.alextabusca.fitmeapi.backend.controller;

import com.alextabusca.fitmeapi.backend.model.UserWorkoutDTO;
import com.alextabusca.fitmeapi.backend.service.UserWorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/userWorkouts")
public class UserWorkoutController {

    private final UserWorkoutService userWorkoutService;

    @Autowired
    public UserWorkoutController(UserWorkoutService userWorkoutService) {
        this.userWorkoutService = userWorkoutService;
    }

    @GetMapping()
    public List<UserWorkoutDTO> getAllUserWorkouts() {
        try {
            return userWorkoutService.getAllUserWorkouts();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/{userWorkoutID}")
    public UserWorkoutDTO getUserWorkoutByID(@PathVariable Long userWorkoutID) {
        try {
            return userWorkoutService.getUserWorkoutByID(userWorkoutID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/byUser={userID}")
    public List<UserWorkoutDTO> getUserWorkoutByUserID(@PathVariable Long userID) {
        try {
            return userWorkoutService.getUserWorkoutByUserID(userID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/byWorkout={workoutID}")
    public List<UserWorkoutDTO> getUserMealByWorkoutID(@PathVariable Long workoutID) {
        try {
            return userWorkoutService.getUserWorkoutByWorkoutID(workoutID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping()
    public UserWorkoutDTO saveUserWorkout(@RequestParam Long userID, @RequestParam Long workoutID) {
        try {
            return userWorkoutService.saveUserWorkout(userID, workoutID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PutMapping()
    public UserWorkoutDTO updateUserWorkout(@RequestParam Long userWorkoutID, @RequestParam Long userID, @RequestParam Long workoutID) {
        try {
            return userWorkoutService.updateUserWorkout(userWorkoutID, userID, workoutID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DeleteMapping()
    public String deleteUserWorkoutByID(@RequestParam Long userWorkoutID) {
        try {
            userWorkoutService.deleteUserWorkoutByID(userWorkoutID);
            return "UserWorkout with id = " + userWorkoutID + "has been successfully deleted";
        } catch (Exception e) {
            e.printStackTrace();
            return "UserWorkout with id = " + userWorkoutID + "could not be deleted";
        }
    }

    @DeleteMapping("/byUserAndWorkout")
    public String deleteUserWorkoutByUserIDAndWorkoutID(@RequestParam Long userID, @RequestParam Long workoutID) {
        try {
            userWorkoutService.deleteUserWorkoutByUserIDAndWorkoutID(userID, workoutID);
            return "UserWorkout has been successfully deleted";
        } catch (Exception e) {
            e.printStackTrace();
            return "UserWorkout could not be deleted";
        }
    }
}
