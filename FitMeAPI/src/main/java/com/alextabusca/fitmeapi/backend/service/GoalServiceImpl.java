package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.GoalModel;
import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.UserModel;
import com.alextabusca.fitmeapi.backend.dataAccess.repository.GoalRepository;
import com.alextabusca.fitmeapi.backend.dataAccess.repository.UserRepository;
import com.alextabusca.fitmeapi.backend.model.GoalDTO;
import com.alextabusca.fitmeapi.backend.model.UserDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GoalServiceImpl implements GoalService {

    private final GoalRepository goalRepository;
    private final UserRepository userRepository;

    public GoalServiceImpl(GoalRepository goalRepository, UserRepository userRepository) {
        this.goalRepository = goalRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<GoalDTO> getAllGoals() {
        try {
            return modelListToDTOList(goalRepository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public GoalDTO getGoalByID(Long goalID) {
        try {
            GoalModel goalModel = goalRepository.getOne(goalID);

            return modelToDTO(goalModel);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<GoalDTO> getGoalsByUserID(Long userID) {
        try {
            UserModel userModel = userRepository.getOne(userID);
            return modelListToDTOList(goalRepository.getGoalModelsByUserModel(userModel));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public GoalDTO saveGoal(GoalDTO goalDTO, Long userID) {
        UserModel userModel = userRepository.getOne(userID);
        GoalModel goalModel = new GoalModel(goalDTO.getTitle(), goalDTO.getDeadline(), userModel);

        try {
            return modelToDTO(goalRepository.save(goalModel));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public GoalDTO updateGoal(Long goalID, GoalDTO goalDTO) {
        GoalModel goalModel = goalRepository.getOne(goalID);

        if (goalDTO.getTitle() != null)
            goalModel.setTitle(goalDTO.getTitle());

        if (goalDTO.getDeadline() != null)
            goalModel.setDeadline(goalDTO.getDeadline());

        try {
            return modelToDTO(goalRepository.save(goalModel));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteGoalByID(Long goalID) {
        goalRepository.deleteById(goalID);
    }

    private GoalDTO modelToDTO(GoalModel goalModel) {


        GoalDTO goalDTO = new GoalDTO();

        UserModel userModel = goalModel.getUserModel();
        UserDTO userDTO = new UserDTO(userModel.getName(), userModel.getEmail(), userModel.getPassword());
        userDTO.setUserID(userModel.getUserID());

        goalDTO.setGoalID(goalModel.getGoalID());
        goalDTO.setTitle(goalModel.getTitle());
        goalDTO.setDeadline(goalModel.getDeadline());
        goalDTO.setUserDTO(userDTO);

        return goalDTO;
    }

    private List<GoalDTO> modelListToDTOList(List<GoalModel> goalModelList) {
        List<GoalDTO> goalDTOList = new ArrayList<>();

        for (GoalModel goalModel : goalModelList)
            goalDTOList.add(modelToDTO(goalModel));

        return goalDTOList;
    }

}
