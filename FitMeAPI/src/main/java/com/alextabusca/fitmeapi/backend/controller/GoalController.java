package com.alextabusca.fitmeapi.backend.controller;

import com.alextabusca.fitmeapi.backend.model.GoalDTO;
import com.alextabusca.fitmeapi.backend.service.GoalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;

@RestController
@RequestMapping("/goals")
public class GoalController {

    private final GoalService goalService;

    @Autowired
    public GoalController(GoalService goalService) {
        this.goalService = goalService;
    }

    @GetMapping()
    public List<GoalDTO> getAllGoals() {
        try {
            return goalService.getAllGoals();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/{goalID}")
    public GoalDTO getGoalByID(@PathVariable Long goalID) {
        try {
            return goalService.getGoalByID(goalID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/byUser={userID}")
    public List<GoalDTO> getGoalsByUserID(@PathVariable Long userID) {
        try {
            return goalService.getGoalsByUserID(userID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping()
    public GoalDTO saveGoal(@RequestParam String title, @RequestParam Date deadline, @RequestParam Long userID) {
        try {
            GoalDTO goalDTO = new GoalDTO(title, deadline);
            return goalService.saveGoal(goalDTO, userID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PutMapping()
    public GoalDTO updateGoal(@RequestParam Long goalID, @RequestParam String title, @RequestParam Date deadline) {
        try {
            GoalDTO goalDTO = new GoalDTO(title, deadline);
            return goalService.updateGoal(goalID, goalDTO);
        } catch (Exception e) {
            return null;
        }
    }

    @DeleteMapping()
    public String deleteGoalByID(@RequestParam Long goalID) {
        try {
            goalService.deleteGoalByID(goalID);
            return "Goal with id = " + goalID + " has been successfully deleted";
        } catch (Exception e) {
            e.printStackTrace();
            return "Goal with id = " + goalID + " could not be deleted";
        }
    }
}
