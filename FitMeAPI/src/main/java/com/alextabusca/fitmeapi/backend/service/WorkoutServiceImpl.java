package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.WorkoutModel;
import com.alextabusca.fitmeapi.backend.dataAccess.repository.WorkoutRepository;
import com.alextabusca.fitmeapi.backend.model.WorkoutDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WorkoutServiceImpl implements WorkoutService {

    private final WorkoutRepository workoutRepository;

    public WorkoutServiceImpl(WorkoutRepository workoutRepository) {
        this.workoutRepository = workoutRepository;
    }

    @Override
    public List<WorkoutDTO> getAllWorkouts() {
        try {
            return modelListToDTOList(workoutRepository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public WorkoutDTO getWorkoutByID(Long workoutID) {
        try {
            WorkoutModel workoutModel = workoutRepository.getOne(workoutID);

            return modelToDTO(workoutModel);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public WorkoutDTO saveWorkout(WorkoutDTO workoutDTO) {
        try {
            WorkoutModel workoutModel = new WorkoutModel(workoutDTO.getTitle());

            return modelToDTO(workoutRepository.save(workoutModel));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public WorkoutDTO updateWorkout(Long workoutID, WorkoutDTO workoutDTO) {
        try {
            WorkoutModel workoutModel = new WorkoutModel(workoutDTO.getTitle());

            if (workoutDTO.getTitle() != null)
                workoutModel.setTitle(workoutDTO.getTitle());

            return modelToDTO(workoutRepository.save(workoutModel));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteWorkoutByID(Long workoutID) {
        workoutRepository.deleteById(workoutID);
    }

    private WorkoutDTO modelToDTO(WorkoutModel workoutModel) {
        WorkoutDTO workoutDTO = new WorkoutDTO(workoutModel.getTitle());
        workoutDTO.setWorkoutID(workoutModel.getWorkoutID());

        return workoutDTO;
    }

    private List<WorkoutDTO> modelListToDTOList(List<WorkoutModel> workoutModelList) {
        List<WorkoutDTO> workoutDTOList = new ArrayList<>();

        for (WorkoutModel workoutModel : workoutModelList)
            workoutDTOList.add(modelToDTO(workoutModel));

        return workoutDTOList;
    }
}
