package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.model.UserWorkoutDTO;

import java.util.List;

public interface UserWorkoutService {

    List<UserWorkoutDTO> getAllUserWorkouts();

    UserWorkoutDTO getUserWorkoutByID(Long userWorkoutID);

    List<UserWorkoutDTO> getUserWorkoutByUserID(Long userID);

    List<UserWorkoutDTO> getUserWorkoutByWorkoutID(Long workoutID);

    UserWorkoutDTO saveUserWorkout(Long userID, Long workoutID);

    UserWorkoutDTO updateUserWorkout(Long userWorkoutID, Long userID, Long workoutID);

    void deleteUserWorkoutByID(Long userWorkoutID);

    void deleteUserWorkoutByUserIDAndWorkoutID(Long userID, Long WorkoutID);
}
