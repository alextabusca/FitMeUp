package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.model.AdminDTO;

import java.util.List;

public interface AdminService {

    List<AdminDTO> getAllAdmins();

    AdminDTO getAdminByID(Long adminID);

    AdminDTO loginAdmin(String email, String password);

    AdminDTO saveAdmin(AdminDTO adminDTO);

    AdminDTO updateAdmin(Long adminID, AdminDTO adminDTO);

    void deleteAdminByID(Long adminID);

}
