package com.alextabusca.fitmeapi.backend.dataAccess.dbModel;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;

@Entity
@Proxy(lazy = false)
@Table(name = "user_workout")
public class UserWorkoutModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_workout_id")
    private Long userWorkoutID;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserModel userModel;

    @ManyToOne
    @JoinColumn(name = "workout_id")
    private WorkoutModel workoutModel;

    public UserWorkoutModel() {
    }

    public UserWorkoutModel(UserModel userModel, WorkoutModel workoutModel) {
        this.userModel = userModel;
        this.workoutModel = workoutModel;
    }

    public Long getUserWorkoutID() {
        return userWorkoutID;
    }

    public void setUserWorkoutID(Long userWorkoutID) {
        this.userWorkoutID = userWorkoutID;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public WorkoutModel getWorkoutModel() {
        return workoutModel;
    }

    public void setWorkoutModel(WorkoutModel workoutModel) {
        this.workoutModel = workoutModel;
    }
}

