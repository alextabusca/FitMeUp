package com.alextabusca.fitmeapi.backend.model;

import java.io.Serializable;

public class WorkoutDTO implements Serializable {

    private Long workoutID;
    private String title;

    public WorkoutDTO() {
    }

    public WorkoutDTO(String title) {
        this.title = title;
    }

    public Long getWorkoutID() {
        return workoutID;
    }

    public void setWorkoutID(Long workoutID) {
        this.workoutID = workoutID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
