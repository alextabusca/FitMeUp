package com.alextabusca.fitmeapi.backend.dataAccess.dbModel;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;

@Entity
@Proxy(lazy = false)
@Table(name = "user_meal")
public class UserMealModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_meal_id")
    private Long userMealID;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserModel userModel;

    @ManyToOne
    @JoinColumn(name = "meal_id")
    private MealModel mealModel;

    public UserMealModel() {
    }

    public UserMealModel(UserModel userModel, MealModel mealModel) {
        this.userModel = userModel;
        this.mealModel = mealModel;
    }

    public Long getUserMealID() {
        return userMealID;
    }

    public void setUserMealID(Long userMealID) {
        this.userMealID = userMealID;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public MealModel getMealModel() {
        return mealModel;
    }

    public void setMealModel(MealModel mealModel) {
        this.mealModel = mealModel;
    }
}
