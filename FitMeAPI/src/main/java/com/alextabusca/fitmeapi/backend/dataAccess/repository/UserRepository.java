package com.alextabusca.fitmeapi.backend.dataAccess.repository;

import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<UserModel, Long> {

    UserModel findByEmailAndPassword(String email, String password);

}
