package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.model.UserDTO;

import java.util.List;

public interface UserService {

    List<UserDTO> getAllUsers();

    UserDTO getUserByID(Long userID);

    UserDTO loginUser(String email, String password);

    UserDTO saveUser(UserDTO userDTO);

    UserDTO updateUser(Long userID, UserDTO userDTO);

    void deleteUserByID(Long userID);

}
