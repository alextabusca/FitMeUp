package com.alextabusca.fitmeapi.backend.dataAccess.repository;

import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.AdminModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface AdminRepository extends JpaRepository<AdminModel, Long> {

    AdminModel findAdminModelByEmailAndPassword(String email, String password);
}
