package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.model.UserMealDTO;

import java.util.List;

public interface UserMealService {

    List<UserMealDTO> getAllUserMeals();

    UserMealDTO getUserMealByID(Long userMealID);

    List<UserMealDTO> getUserMealByUserID(Long userID);

    List<UserMealDTO> getUserMealByMealID(Long mealID);

    UserMealDTO saveUserMeal(Long userID, Long mealID);

    UserMealDTO updateUserMeal(Long userMealID, Long userID, Long mealID);

    void deleteUserMealByID(Long userMealID);

    void deleteUserMealByUserIDAndMealID(Long userID, Long mealID);
}
