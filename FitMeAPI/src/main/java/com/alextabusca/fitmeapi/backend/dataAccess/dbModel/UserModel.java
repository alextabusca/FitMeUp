package com.alextabusca.fitmeapi.backend.dataAccess.dbModel;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Set;

@Entity
@Proxy(lazy = false)
@Table(name = "user")
public class UserModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long userID;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @JsonIgnore
    @JsonBackReference
    @OneToMany(mappedBy = "userModel", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<GoalModel> goalModels;

    @JsonIgnore
    @JsonBackReference
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_meal", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "meal_id", referencedColumnName = "meal_id")})
    private Set<MealModel> userUserMeal;

    @JsonIgnore
    @JsonBackReference
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_workout", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "workout_id", referencedColumnName = "workout_id")})
    private Set<WorkoutModel> userUserWorkout;

    public UserModel() {
    }

    public UserModel(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<GoalModel> getGoalModels() {
        return goalModels;
    }

    public void setGoalModels(Set<GoalModel> goalModels) {
        this.goalModels = goalModels;
    }

    public Set<MealModel> getUserUserMeal() {
        return userUserMeal;
    }

    public void setUserUserMeal(Set<MealModel> userUserMeal) {
        this.userUserMeal = userUserMeal;
    }

    public Set<WorkoutModel> getUserUserWorkout() {
        return userUserWorkout;
    }

    public void setUserUserWorkout(Set<WorkoutModel> userUserWorkout) {
        this.userUserWorkout = userUserWorkout;
    }
}
