package com.alextabusca.fitmeapi.backend.controller;

import com.alextabusca.fitmeapi.backend.model.AdminDTO;
import com.alextabusca.fitmeapi.backend.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admins")
public class AdminController {

    private final AdminService adminService;

    @Autowired
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping()
    public List<AdminDTO> getAllAdmins() {
        try {
            return adminService.getAllAdmins();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/{adminID}")
    public AdminDTO getAdminByID(@PathVariable Long adminID) {
        try {
            return adminService.getAdminByID(adminID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/{email}/{password}")
    public AdminDTO loginAdmin(@PathVariable String email, @PathVariable String password) {
        try {
            return adminService.loginAdmin(email, password);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping()
    public AdminDTO saveAdmin(@RequestParam String name, @RequestParam String email, @RequestParam String password) {
        try {
            AdminDTO adminDTO = new AdminDTO(name, email, password);
            return adminService.saveAdmin(adminDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PutMapping()
    public AdminDTO updateAdmin(@RequestParam Long adminID, @RequestParam String name, @RequestParam String email, @RequestParam String password) {
        try {
            AdminDTO adminDTO = new AdminDTO(name, email, password);
            return adminService.updateAdmin(adminID, adminDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DeleteMapping()
    public String deleteAdminByID(@RequestParam Long adminID) {
        try {
            adminService.deleteAdminByID(adminID);
            return "Admin with id = " + adminID + " has been successfully deleted";
        } catch (Exception e) {
            e.printStackTrace();
            return "Admin with id = " + adminID + " could not be deleted";
        }
    }
}
