package com.alextabusca.fitmeapi.backend.model;

import java.io.Serializable;
import java.sql.Date;

public class GoalDTO implements Serializable {

    private Long goalID;
    private String title;
    private Date deadline;
    private UserDTO userDTO;

    public GoalDTO() {
    }

    public GoalDTO(String title, Date deadline) {
        this.title = title;
        this.deadline = deadline;
    }

    public GoalDTO(String title, Date deadline, UserDTO userDTO) {
        this.title = title;
        this.deadline = deadline;
        this.userDTO = userDTO;
    }

    public Long getGoalID() {
        return goalID;
    }

    public void setGoalID(Long goalID) {
        this.goalID = goalID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
