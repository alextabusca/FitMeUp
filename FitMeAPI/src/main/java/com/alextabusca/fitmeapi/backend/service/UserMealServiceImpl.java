package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.MealModel;
import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.UserMealModel;
import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.UserModel;
import com.alextabusca.fitmeapi.backend.dataAccess.repository.MealRepository;
import com.alextabusca.fitmeapi.backend.dataAccess.repository.UserMealRepository;
import com.alextabusca.fitmeapi.backend.dataAccess.repository.UserRepository;
import com.alextabusca.fitmeapi.backend.model.MealDTO;
import com.alextabusca.fitmeapi.backend.model.UserDTO;
import com.alextabusca.fitmeapi.backend.model.UserMealDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserMealServiceImpl implements UserMealService {

    private final UserMealRepository userMealRepository;
    private final UserRepository userRepository;
    private final MealRepository mealRepository;

    @Autowired
    public UserMealServiceImpl(UserMealRepository userMealRepository, UserRepository userRepository, MealRepository mealRepository) {
        this.userMealRepository = userMealRepository;
        this.userRepository = userRepository;
        this.mealRepository = mealRepository;
    }


    @Override
    public List<UserMealDTO> getAllUserMeals() {
        try {
            return modelListToDTOList(userMealRepository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserMealDTO getUserMealByID(Long userMealID) {
        try {
            UserMealModel userMealModel = userMealRepository.getOne(userMealID);

            return modelToDTO(userMealModel);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<UserMealDTO> getUserMealByUserID(Long userID) {
        try {
            List<UserMealModel> userList;

            UserModel userModel = userRepository.getOne(userID);
            userList = userMealRepository.findUserMealModelByUserModel(userModel);

            return modelListToDTOList(userList);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<UserMealDTO> getUserMealByMealID(Long mealID) {
        try {
            List<UserMealModel> mealList;

            MealModel mealModel = mealRepository.getOne(mealID);
            mealList = userMealRepository.findUserMealModelByMealModel(mealModel);

            return modelListToDTOList(mealList);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserMealDTO saveUserMeal(Long userID, Long mealID) {
        try {
            UserModel userModel = userRepository.getOne(userID);
            MealModel mealModel = mealRepository.getOne(mealID);

            userModel.getUserUserMeal().add(mealModel);
            mealModel.getMealUserMeal().add(userModel);

            UserMealModel userMealModel = new UserMealModel(userModel, mealModel);

            return modelToDTO(userMealRepository.save(userMealModel));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserMealDTO updateUserMeal(Long userMealID, Long userID, Long mealID) {
        try {
            UserMealModel userMealModel = userMealRepository.getOne(userMealID);
            UserModel userModel = userRepository.getOne(userID);
            MealModel mealModel = mealRepository.getOne(mealID);

            userMealModel.setUserModel(userModel);
            userMealModel.setMealModel(mealModel);

            return modelToDTO(userMealRepository.save(userMealModel));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteUserMealByID(Long userMealID) {
        try {
            userMealRepository.deleteById(userMealID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteUserMealByUserIDAndMealID(Long userID, Long mealID) {
        try {
            UserModel userModel = userRepository.getOne(userID);
            MealModel mealModel = mealRepository.getOne(mealID);

            userMealRepository.deleteUserMealModelByUserModelAndMealModel(userModel, mealModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private UserMealDTO modelToDTO(UserMealModel userMealModel) {
        UserMealDTO userMealDTO = new UserMealDTO();

        UserModel userModel = userMealModel.getUserModel();
        MealModel mealModel = userMealModel.getMealModel();

        UserDTO userDTO = new UserDTO(userModel.getName(), userModel.getEmail(), userModel.getPassword());
        userDTO.setUserID(userModel.getUserID());

        MealDTO mealDTO = new MealDTO(mealModel.getTitle(), mealModel.getIngredients(), mealModel.getPreparation());
        mealDTO.setMealID(mealModel.getMealID());

        userMealDTO.setUserDTO(userDTO);
        userMealDTO.setMealDTO(mealDTO);
        userMealDTO.setUserMealID(userMealModel.getUserMealID());

        return userMealDTO;
    }

    private List<UserMealDTO> modelListToDTOList(List<UserMealModel> userMealModelList) {
        List<UserMealDTO> userMealDTOList = new ArrayList<>();

        for (UserMealModel userMealModel : userMealModelList)
            userMealDTOList.add(modelToDTO(userMealModel));

        return userMealDTOList;
    }
}
