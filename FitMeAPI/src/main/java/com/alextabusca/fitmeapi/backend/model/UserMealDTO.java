package com.alextabusca.fitmeapi.backend.model;

import java.io.Serializable;

public class UserMealDTO implements Serializable {

    private Long userMealID;
    private UserDTO userDTO;
    private MealDTO mealDTO;

    public UserMealDTO() {
    }

    public UserMealDTO(UserDTO userDTO, MealDTO mealDTO) {
        this.userDTO = userDTO;
        this.mealDTO = mealDTO;
    }

    public UserMealDTO(Long userMealID, UserDTO userDTO, MealDTO mealDTO) {
        this.userMealID = userMealID;
        this.userDTO = userDTO;
        this.mealDTO = mealDTO;
    }

    public Long getUserMealID() {
        return userMealID;
    }

    public void setUserMealID(Long userMealID) {
        this.userMealID = userMealID;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public MealDTO getMealDTO() {
        return mealDTO;
    }

    public void setMealDTO(MealDTO mealDTO) {
        this.mealDTO = mealDTO;
    }
}
