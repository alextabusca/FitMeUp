package com.alextabusca.fitmeapi.backend.dataAccess.repository;

import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.UserModel;
import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.UserWorkoutModel;
import com.alextabusca.fitmeapi.backend.dataAccess.dbModel.WorkoutModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface UserWorkoutRepository extends JpaRepository<UserWorkoutModel, Long> {

    List<UserWorkoutModel> findUserWorkoutModelByUserModel(UserModel userModel);

    List<UserWorkoutModel> findUserWorkoutModelByWorkoutModel(WorkoutModel workoutModel);

    void deleteUserWorkoutModelByUserModelAndWorkoutModel(UserModel userModel, WorkoutModel workoutModel);
}
