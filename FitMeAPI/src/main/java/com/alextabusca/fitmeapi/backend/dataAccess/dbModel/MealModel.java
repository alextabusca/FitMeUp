package com.alextabusca.fitmeapi.backend.dataAccess.dbModel;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Set;

@Entity
@Proxy(lazy = false)
@Table(name = "meal")
public class MealModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "meal_id")
    private Long mealID;

    @Column(name = "title")
    private String title;

    @Column(name = "ingredients")
    private String ingredients;

    @Column(name = "preparation")
    private String preparation;

    @JsonIgnore
    @JsonBackReference
    @ManyToMany(mappedBy = "userUserMeal", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<UserModel> mealUserMeal;

    public MealModel() {
    }

    public MealModel(String title, String ingredients, String preparation) {
        this.title = title;
        this.ingredients = ingredients;
        this.preparation = preparation;
    }

    public Long getMealID() {
        return mealID;
    }

    public void setMealID(Long mealID) {
        this.mealID = mealID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    public Set<UserModel> getMealUserMeal() {
        return mealUserMeal;
    }

    public void setMealUserMeal(Set<UserModel> mealUserMeal) {
        this.mealUserMeal = mealUserMeal;
    }
}
