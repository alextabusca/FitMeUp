package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.model.GoalDTO;

import java.util.List;

public interface GoalService {

    List<GoalDTO> getAllGoals();

    GoalDTO getGoalByID(Long goalID);

    List<GoalDTO> getGoalsByUserID(Long userID);

    GoalDTO saveGoal(GoalDTO goalDTO, Long userID);

    GoalDTO updateGoal(Long goalID, GoalDTO goalDTO);

    void deleteGoalByID(Long goalID);

}
