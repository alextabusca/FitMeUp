package com.alextabusca.fitmeapi.backend.service;

import com.alextabusca.fitmeapi.backend.model.WorkoutDTO;

import java.util.List;

public interface WorkoutService {

    List<WorkoutDTO> getAllWorkouts();

    WorkoutDTO getWorkoutByID(Long workoutID);

    WorkoutDTO saveWorkout(WorkoutDTO workoutDTO);

    WorkoutDTO updateWorkout(Long workoutID, WorkoutDTO workoutDTO);

    void deleteWorkoutByID(Long workoutID);

}
